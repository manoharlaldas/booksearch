Rails.application.routes.draw do


  devise_for :users
  # devise_for :users, controllers: { sessions: 'users/sessions' }

  resources :users, only: [:show]

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  resources :line_items
  resources :carts
  resources :store
	resources :books
	resources :categories
  resources :orders


  resources :line_items do
        get 'decrement', on: :member
  end

  resources :orders

  get 'get-cart', action: :get_cart, controller: :carts

	root 'store#index'
  get '/about',   :to => 'store#about'
  get '/privacy',   :to => 'store#privacy'
  get '/contact',   :to => 'store#contact'
  get '/termandcondition',   :to => 'store#termandcondition'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
