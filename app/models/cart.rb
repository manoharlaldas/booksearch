class Cart < ApplicationRecord
	has_many :line_items, dependent: :destroy
  has_one :user

 def add_book(book, quantity=1)
  current_item = line_items.find_by(book_id: book.id)
  if current_item
    current_quantity = current_item.quantity
    quantity = current_quantity.to_i + quantity.to_i
    current_item.update_attribute(:quantity, quantity)
  else
    current_item = line_items.build(book_id: book.id, quantity: quantity)
  end
  current_item
 end

  def total_price
    line_items.to_a.sum { |item| item.total_price }
  end
end
