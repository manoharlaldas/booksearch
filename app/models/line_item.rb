class LineItem < ApplicationRecord
  belongs_to :book
  belongs_to :cart


  belongs_to :order, optional: true

  def total_price
  	book.price.to_i * quantity.to_i
  end
end
