class Book < ApplicationRecord
	before_destroy :not_referenced_by_any_line_item
	mount_uploader :image, ImageUploader
	belongs_to :category
	has_many :line_items
	validates :title, presence: true
	validates :author, presence: true
	validates :price, presence: true
	validates :description, presence: true, length: { maximum: 1000, too_long: "%{count} characters is the maximum aloud. "}

	validates_processing_of :image
	validate :image_size_validation

	private
	  def image_size_validation
	    errors[:image] << "should be less than 500KB" if image.size > 0.5.megabytes
	  end
    
    private

	  def not_refereced_by_any_line_item
	    unless line_items.empty?
	      errors.add(:base, "Line items present")
	    throw :abort
	  end
  end
end
