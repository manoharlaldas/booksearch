module ApplicationHelper

	# helper_method :resource_name, :resource, :devise_mapping, :resource_class

	  def resource_name
	    :user
	  end
	 
	  def resource
	    @resource ||= User.new
	  end

	  def resource_class
	    User
	  end
	 
	  def devise_mapping
	    @devise_mapping ||= Devise.mappings[:user]
	  end

	
	def cart_count_over_one
	    if @cart.line_items.count > 0
	      return "<span class='tag is-dark'>#{@cart.line_items.count}</span>".html_safe
	    end
	end

  def cart_has_items
    return @cart.line_items.count > 0
  end

end
