class CategoriesController < ApplicationController
  def index
  	@categories = Category.all
  end

  def new
  	@category = Category.new
  end

  def create
  	@category = Category.new(category_params)
  	if @category.save
  		redirect_to categories_path, notice: "Created Successfully"
  	else
  		render 'new'
  	end
  end

  def edit
  	@category = Category.find(params[:id])
  end
  
  def update
  	@category = Category.find(params[:id])
  	if @category.update_attributes(category_params)
  		redirect_to categories_path, notice: "Updated Successfully"
  	else
  		render "edit"
  	end
  end

  def destroy
  	@category = Category.find(params[:id])
  	if @category.destroy
  		redirect_to categories_path, notice: "Deleted category"
  	end
  end

  private

  def category_params
  	params.require(:category).permit(:title)
  end
end
