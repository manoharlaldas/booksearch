class LineItemsController < ApplicationController

  # POST /line_items
  # POST /line_items.json
  def create
    @book = Book.find(params[:book_id])
    @quantity = params[:quantity]
    @line_item = @cart.add_book(@book, @quantity)

    respond_to do |format|
      if @line_item.save
        format.html { redirect_to root_path }
        format.js
        format.json { render :show, status: :created, location: @line_item }
      else
        flash[:success] = @line_item.errors
        format.html { redirect_to root_path }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def decrement
    @line_item = LineItem.find(params[:id])
    @line_item.destroy
    respond_to do |format|
      format.html { redirect_to root_path }
      format.js
      format.json { head :no_content }
    end
  end

  private
    def line_item_params

      params.require(:line_item).permit(:book_id, :quantity)

    end
end
