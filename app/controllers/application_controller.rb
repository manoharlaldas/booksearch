class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include CategoryList
  
  before_action :set_cart
  before_action :set_categories
  before_action :configure_sign_up_params, only: [:create], if: :devise_controller?
  before_action :configure_account_update_params, only: [:update], if: :devise_controller?

  after_action :store_action
  
  def store_action
    return unless request.get? 
    if (request.path != "/users/sign_in" &&
        request.path != "/users/sign_up" &&
        request.path != "/users/password/new" &&
        request.path != "/users/password/edit" &&
        request.path != "/users/confirmation" &&
        request.path != "/users/sign_out" &&
        !request.xhr?) # don't store ajax calls
      store_location_for(:user, request.fullpath)
    end
  end


  protected
  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :address, :image, :telephone])
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :address, :image, :telephone, :cart_id])
  end

  # def after_sign_in_path_for(resource_or_scope)
  #   if resource_or_scope.is_a?(User)
  #     store_index_path
  #   elsif resource_or_scope.is_a?(AdminUser) 
  #     admin_dashboard_path
  #   end
  # end

  # def after_sign_out_path_for(users)
  #   store_index_path
  # end

  # def after_sign_in_path_for(users)
  #   current_user
  # end
  

  private

  # user has one cart_id
  # current_user[:cart_id] @cart.id


  # def set_cart
  #   @cart = Cart.find(session[:cart_id])
  #   rescue ActiveRecord::RecordNotFound
  #   @cart = Cart.create
  #   session[:cart_id] = @cart.id
  # end

  # def set_cart
  #   if user_signed_in?
  #     @cart = Cart.find(current_user[:cart_id]) if current_user[:cart_id].present?
  #     if @cart.present?
  #       @cart
  #     else
  #       @cart = Cart.create
  #       current_user[:cart_id] = @cart.id
  #       current_user.save
  #       @cart
  #     end

  #   else
  #     @cart = Cart.find(session[:cart_id]) if session[:cart_id].present?
  #     if @cart.nil?
  #       @cart = Cart.create
  #       session[:cart_id] = @cart.id
  #     end
  #   end
  # end

  def set_cart
    if user_signed_in?
      @session_cart_items = Cart.find(session[:cart_id]).line_items
      @cart = Cart.find(current_user[:cart_id]) if current_user[:cart_id].present?
      if @cart.present?
        current_user.cart.line_items.concat(@session_cart_items)
        @cart = Cart.find(current_user.cart_id)
      else
        @cart = Cart.create
        current_user[:cart_id] = @cart.id
        current_user.save
        current_user.cart.line_items.concat(@session_cart_items)
        @cart = Cart.find(current_user.cart_id)
      end

    else
      @cart = Cart.find(session[:cart_id]) if session[:cart_id].present?
      if @cart.nil?
        @cart = Cart.create
        session[:cart_id] = @cart.id
      end
    end
  end


# def set_cart
#   if user_signed_in?
#     @cart = Cart.find(current_user[:cart_id]) if current_user[:cart_id].present?
#     if @cart.present?
#       @cart
#     else
#       @cart = Cart.create
#       current_user[:cart_id] = @cart.id
#       current_user.save
#       @cart_id
#     end
#   else
#     @cart = Cart.find(session[:cart_id]) if session[:cart_id].present?
#     if @cart.nil?
#       @cart = Cart.create
#       session[:cart_id] = @cart.id
#     end
#   end
# end
  # def set_cart
  #   @cart = Cart.find(session[:cart_id]) || Cart.find(current_user[:cart_id])
  #   rescue ActiveRecord::RecordNotFound
  #   @cart = Cart.create
  #   if user_signed_in?
  #     session[:cart_id] = nil
  #     current_user[:cart_id] = @cart.id
  #     current_user.save
  #   else
  #     session[:cart_id] = @cart.id
  #   end
  # end
end