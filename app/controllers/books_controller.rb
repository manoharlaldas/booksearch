class BooksController < ApplicationController
  # before_action :authenticate_user!
  before_action :set_book, only: [:show, :edit, :update, :destroy]
  # before_action :authenticate_user!, except: [:index, :show]
  def index
    @books = Book.paginate(:page => params[:page], :per_page => 3)
  end

  def show
  end

  def new
    # @book = current_user.books.build
    @book = Book.new
    # respond_to do |format|
    #   format.html
    # end
  end
  
  def edit
  end

  def create
    # @book = current_user.books.build(book_params)
    @book = Book.new(book_params)
    respond_to do |format|
      if @book.save
        format.html { redirect_to books_path, notice: 'Book was successfully created.'}
        format.json { render :show, status: :created, location: @book }
      else
        format.html { render :new }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
    # render plain: book_params.inspect
  end

  def update
    respond_to do |format|
      if @book.update(book_params)
        flash[:notice] = 'Book was successfully updated'
        format.html { redirect_to @book, notice: 'Book was successfully updated' }
        format.json { render :show, status: :created, location: @book }
      else
        format.html { render :edit }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @book.destroy
    flash[:success] = 'Book destroyed.'
    redirect_to books_path
  end

  private

  def set_book
    @book = Book.find(params[:id])
  end

  def book_params
    params.require(:book).permit(:title, :release_year, :price, :description, :isbn, :publisher, :author, :category_id, :image, :remove_image)
  end
end
