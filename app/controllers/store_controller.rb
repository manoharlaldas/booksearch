class StoreController < ApplicationController
  # before_action :authenticate_user!

  def index
  	if params[:category_id]
  		category_id = params[:category_id]
  		category = Category.find(category_id)
  		@books = category.books.paginate(:page => params[:page], :per_page => 12)
  	else
	  	@books = Book.paginate(:page => params[:page], :per_page => 12)
	end
  	@categories = Category.all
  end


  def show
  	@book = Book.find(params[:id])
  end
end
