class OrdersController < ApplicationController
  before_action :authenticate_user!
  def index
  	@orders = Order.all
  end

  def new
  	@cart = Cart.find(params[:cart_id])
	  @order = Order.new
  end

  def create
  	@order = current_user.orders.new()
    @order.add_line_items_from_cart(@cart)
    if @order.save
      redirect_to store_index_path, notice: "thanks for your order"
    else
      redirect_to store_index_path, notice: @order.errors
    end
  end

  private

  def order_params
 	params.require(:order).permit(:user_id, :pay_type)
  end
end
