class CartsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :invalid_cart

  # def destroy
  #   @cart.destroy if @cart.id == session[:cart_id]
  #   session[:cart_id] = nil
  #   flash[:info] = 'Cart was successfully destroyed.'
  #   respond_to do |format|
  #     format.html { redirect_to root_path }
  #     format.json { head :no_content }
  #   end
  # end


  def destroy
    if @cart.id == session[:cart_id]
      @cart.destroy 
      session[:cart_id] = nil
      flash[:info] = 'Cart was successfully destroyed.'

    elsif @cart.id == current_user[:cart_id]
      @cart.destroy
      current_user[:cart_id] = nil
      current_user.save
      flash[:info] = 'Your personal Cart was successfully destroyed.'
    end
    respond_to do |format|
      format.html {redirect_to root_path}
      format.json{ head :no_content }
    end
  end


  def get_cart
    
  end


  private
    def cart_params
      params.fetch(:cart, {})
    end

    def invalid_cart
      logger.error "Attempt to access invalid cart #{params[:id]}"
      redirect_to root_path, notice: "That cart doesn't exist"
    end
end
