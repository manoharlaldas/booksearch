# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Category.create(title: 'Satire')
Category.create(title: 'Science fiction')
Category.create(title: 'Drama')
Category.create(title: 'Action and Adventure')
Category.create(title: 'Romance')
Category.create(title: 'Mystery')
Category.create(title: 'Horror')
Category.create(title: 'Self help')
Category.create(title: 'Health')
Category.create(title: 'Guide')
Category.create(title: 'Travel')
Category.create(title: "Children's")
Category.create(title: "Religion, Spirituality & New Age")
Category.create(title: 'History')
Category.create(title: 'Math')
Category.create(title: 'Poetry')
Category.create(title: 'Art')

