class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
    	t.string :title
    	t.integer :release_year
    	t.float :price
    	t.text :description
    	t.integer :isbn
    	t.string :publisher
    	t.string :author

      t.timestamps
    end
  end
end
